## Magento 2 Trained Partner Program: Developer Exercises 

# Unit Three. Layout

For the controller created in Exercise 1, from the Controllers section add an interface so that if 
the parameter ?json=1 is not set, the page should render a normal 3-column UI with order status, 
total, and total invoiced amount on the page instead of returning the json. (Training3_OrderInfo)

Go to: [http://mage2.local/m2t3orderinfo/index/index/orderID/1](http://mage2.local/m2t3orderinfo/index/index/orderID/1)
Go to: [http://mage2.local/m2t3orderinfo/index/index/orderID/1?json=1](http://mage2.local/m2t3orderinfo/index/index/orderID/1?json=1)
