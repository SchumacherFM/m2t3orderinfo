<?php

namespace SchumacherFM\M2T3OrderInfo\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

use SchumacherFM\M2T3OrderInfo\Model\SalesOrderInfo;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{
    /**
     * @var SalesOrderInfo
     */
    protected $salesOrderInfo;


    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @param Context                                          $context
     * @param SalesOrderInfo                                   $salesOrder
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        SalesOrderInfo $salesOrder,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->salesOrderInfo = $salesOrder;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isJSONRequest()
    {
        return (int)$this->getRequest()->getParam('json', 0) === 1;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $id = (int)$this->getRequest()->getParam('orderID', 0);

        if (true === $this->_isJSONRequest()) {
            /** @var \Magento\Framework\Controller\Result\JsonFactory $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            $resultJson->setData($this->salesOrderInfo->getOrder($id));
            return $resultJson;
        }

        /** @var \Magento\Framework\View\Result\Page resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}
