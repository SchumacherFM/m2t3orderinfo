<?php


namespace SchumacherFM\M2T3OrderInfo\Model;

use Magento\Sales\Api\OrderRepositoryInterface;

class SalesOrderInfo
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $salesOrder;

    /**
     * SalesOrderInfo constructor.
     *
     * @param OrderRepositoryInterface $orderRepo
     */
    public function __construct(OrderRepositoryInterface $orderRepo)
    {
        $this->salesOrder = $orderRepo;
    }

    public function getOrder($id)
    {

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->salesOrder->get($id);

        $items = [];
        foreach ($order->getItems() as $item) {
            /** @var $item \Magento\Sales\Model\Order\Item */
            $items[] = [
                'sku'     => $item->getSku(),
                'item_id' => $item->getId(),
                'price'   => $item->getPriceInclTax(),
            ];
        }

        $json = [
            'status'         => $order->getStatus(),
            'total'          => $order->getGrandTotal(),
            'total_invoiced' => $order->getTotalInvoiced(),
            'items'          => $items,
        ];
        return $json;
    }
}
