<?php

namespace SchumacherFM\M2T3OrderInfo\Block;

use SchumacherFM\M2T3OrderInfo\Model\SalesOrderInfo;


class OrderJSON extends \Magento\Framework\View\Element\Template
{

    /**
     * @var SalesOrderInfo
     */
    protected $salesOrderInfo;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param SalesOrderInfo                                   $salesOrderInfo
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        SalesOrderInfo $salesOrderInfo,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->salesOrderInfo = $salesOrderInfo;
    }

    /**
     * @return array
     */
    public function getSalesOrder()
    {
        $id = (int)$this->getRequest()->getParam('orderID', 0);
        return $this->salesOrderInfo->getOrder($id);
    }
}
